/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.vertx;

import io.vertx.rxjava.core.Vertx;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class VertxActivator extends DependencyActivatorBase{

	private Vertx m_vertx;

	@Override
	public void init(BundleContext bc, DependencyManager dm) throws Exception {
		m_vertx = Vertx.vertx();
		dm.add(createComponent().setInterface(Vertx.class.getName(), null).setImplementation(m_vertx));
	}
	
	@Override
	public void stop(BundleContext context) throws Exception {
		super.stop(context);
		m_vertx.close();
	}

}
