package org.amdatu.deploymentctl.web.impl.ws;

import static org.amdatu.deploymentctl.web.WebConstants.WS_SERVLET_NAME;
import static org.amdatu.deploymentctl.web.WebConstants.CONTEXT_SELECT_FILTER;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_NAME;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_SERVLET_PATTERN;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Component(provides = Object.class)
@Property(name = WEBSOCKET_NAME, value = WS_SERVLET_NAME)
@Property(name = WEBSOCKET_SERVLET_PATTERN, value = "/websocket/*")
@Property(name = HTTP_WHITEBOARD_CONTEXT_SELECT, value = CONTEXT_SELECT_FILTER)
public class WebsocketServlet {

}
