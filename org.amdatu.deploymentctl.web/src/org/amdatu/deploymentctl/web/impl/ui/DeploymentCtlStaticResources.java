package org.amdatu.deploymentctl.web.impl.ui;

import static org.amdatu.deploymentctl.web.WebConstants.PUBLIC_CONTEXT_SELECT_FILTER;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Pattern;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

/**
 * Servlet for the static DeploymentCtl resources.
 *
 * Files are served from the <code>webui/dist</code> folder with a fallback for
 * local development to <code>../../deploymentctl-ui/dist</code>
 *
 * Adds Cache-Control headers to the response:
 * <ul>
 * <li><b>index.html, keycloak/keycloak.json</b><br />
 * <code>Cache-Control: no-cache</code></li>
 * <li><b>*.bundle.js, *.chunk.js</b><br />
 * <code>Cache-Control: max-age=31536000</code> (one year)</li>
 * <li><b>All other:</b><br />
 * <code>Cache-Control: max-age=86400</code> (one day)</li>
 * </ul>
 */
@Component(provides = Servlet.class)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_SERVLET_PATTERN, value = "/*")
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, value = PUBLIC_CONTEXT_SELECT_FILTER)
public class DeploymentCtlStaticResources extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String HEADER_CACHE_CONTROL = "Cache-Control";
	private static final String HEADER_LAST_MODIFIED = "Last-Modified";
	private static final String HEADER_IF_MODIFIED_SINCE = "If-Modified-Since";

	private static final String CACHE_CONTROL_ONE_DAY = "public, max-age=86400";
	private static final String CACHE_CONTROL_FOREVER = "public, max-age=31536000";
	private static final String CACHE_CONTROL_NO_CACHE = "no-cache";

	// Pattern for resources that should not be cached
	private static final Pattern NO_CACHE_PATTERN = Pattern.compile("\\/(index\\.html|keycloak\\/keycloak\\.json)");
	// Pattern for resources that can be cached forever
	private static final Pattern CACHE_FOREVER_PATTERN = Pattern.compile("\\/.*\\.(bundle|chunk)\\.js");

	private final File m_webResourceDir;

	public DeploymentCtlStaticResources() {
		File webResourceDir = new File("webroot/dist");
		File localWebResourceDir = new File("../../deploymentctl-ui/dist");

		if (!webResourceDir.isDirectory() && localWebResourceDir.isDirectory()) {
			// Fall back to an alternative location for development (only if the production
			// dir doesn't exist)
			m_webResourceDir = localWebResourceDir;
		} else {
			m_webResourceDir = webResourceDir;
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getPathInfo();
		
		// poor man's default page
		if (name == null || name.equals("/") || name.equals("//")) {
			name = "/index.html";
		}

		URL resource = getResource(name);
		if (resource == null) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		String mimeType = getServletContext().getMimeType(name);
		if (mimeType != null) {
			response.setContentType(mimeType);
		}

		URLConnection openConnection = resource.openConnection();
		long lastModified = openConnection.getLastModified();

		long ifModifiedSince = request.getDateHeader(HEADER_IF_MODIFIED_SINCE);
		if (ifModifiedSince != -1 && lastModified != 0 && lastModified / 1000 > ifModifiedSince / 1000) {
			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
		} else {
			if (NO_CACHE_PATTERN.matcher(name).matches()) {
				response.setHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_NO_CACHE);
			} else if (CACHE_FOREVER_PATTERN.matcher(name).matches()) {
				response.setHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_FOREVER);
			} else {
				response.setHeader(HEADER_CACHE_CONTROL, CACHE_CONTROL_ONE_DAY);
			}

			if (lastModified != 0) {
				response.setDateHeader(HEADER_LAST_MODIFIED, lastModified);
			}

			int contentLength = openConnection.getContentLength();
			if (contentLength >= 0) {
				response.setContentLength(contentLength);
			}

			try (InputStream is = openConnection.getInputStream(); OutputStream os = response.getOutputStream();) {

				byte[] buf = new byte[4096];
				int n;

				while ((n = is.read(buf, 0, buf.length)) >= 0) {
					os.write(buf, 0, n);
				}
			}
		}
	}

	public URL getResource(String name) {
		File file = new File(m_webResourceDir, name);

		if (file.exists()) {
			try {
				return file.toURI().toURL();
			} catch (MalformedURLException e) {
				return null; // Should not happen for a file
			}
		}
		return null;
	}

}
