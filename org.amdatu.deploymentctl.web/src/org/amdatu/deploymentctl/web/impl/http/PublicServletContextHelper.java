package org.amdatu.deploymentctl.web.impl.http;

import static org.amdatu.deploymentctl.web.WebConstants.PUBLIC_CONTEXT_NAME;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.framework.Constants;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;

@Component(provides = ServletContextHelper.class)
@Property(name = Constants.SERVICE_RANKING, intValue = 1)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, value = PUBLIC_CONTEXT_NAME)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, value = "/")
public class PublicServletContextHelper extends ServletContextHelper {

}
