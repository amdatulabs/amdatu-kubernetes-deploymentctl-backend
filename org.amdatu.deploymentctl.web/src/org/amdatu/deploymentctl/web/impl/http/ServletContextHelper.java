package org.amdatu.deploymentctl.web.impl.http;

import static org.amdatu.deploymentctl.web.WebConstants.CONTEXT_NAME;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.deploymentctl.configuration.Config;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.Constants;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.amdatu.deploymentctl.web.auth.AuthUtils;
import org.amdatu.deploymentctl.web.auth.WebUser;

import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.oauth2.impl.crypto.TokenVerifier;

@Component(provides = org.osgi.service.http.context.ServletContextHelper.class)
//Make sure this one is ranked higher as the PublicServletContextHelper has a '/*' servlet
@Property(name = Constants.SERVICE_RANKING, intValue = 2)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, value = CONTEXT_NAME)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, value = "/deploymentctl")
public class ServletContextHelper extends org.osgi.service.http.context.ServletContextHelper {

	private final Logger logger = LoggerFactory.getLogger(ServletContextHelper.class);

	@ServiceDependency
	private volatile Config config;

	@Override
	public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
		cors(request, response);

		String jwt = getJwtToken(request);

		if(jwt != null) {
			TokenVerifier tokenVerifier = new TokenVerifier(config.keyCloakPublicKey);
			JsonObject verify = tokenVerifier.verify(jwt);

			if (!verify.isEmpty()) {
				WebUser webuser = new WebUser(verify);
				request.setAttribute(WebUser.class.getName(), webuser);
				request.setAttribute(org.osgi.service.http.context.ServletContextHelper.REMOTE_USER, verify.getValue("preferred_username"));

				// add roles to HttpSession, will be used in WebsocketConfigurator
				List<String> roles = AuthUtils.getRoles(webuser);
				request.getSession().setAttribute("roles", roles);

				return true;
			} else {
				logger.error("could not verify JWT!");
			}
		}

		return AuthUtils.authDisabled() ||
				// TODO: Not sure if this is the best thing to do but preflight requests don't have auth headers / cookies
				request.getMethod().equals("OPTIONS");
	}

	private String getJwtToken(HttpServletRequest request) {
		String jwt = null;

		String authHeader = request.getHeader("Authorization");
		if(authHeader != null) {
			jwt = authHeader.substring(authHeader.indexOf(" ") + 1);
		} else {
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if ("deploymentctl-token".equals(cookie.getName())) {
						jwt = cookie.getValue();
						break;
					}
				}
			}
		}
		return jwt;
	}

	private void cors(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", "Origin, Content-Type, If-Match, Authorization");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        response.setHeader("Access-Control-Allow-Origin", "*"); // TODO add this to config like in dashboard: config.corsAllowedOrigin
        response.setHeader("Access-Control-Expose-Headers", "Link, Location, ETag, Authorization");
	}

}
