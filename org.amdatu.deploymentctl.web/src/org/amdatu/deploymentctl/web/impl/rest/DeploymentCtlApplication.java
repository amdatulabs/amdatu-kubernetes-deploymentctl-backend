package org.amdatu.deploymentctl.web.impl.rest;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.amdatu.deploymentctl.web.WebConstants;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

@Component(provides = Application.class)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_NAME, value = WebConstants.JAX_RS_APPLICATION_NAME)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE, value = "/rest")
@Property(name = AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT, value = WebConstants.CONTEXT_NAME)
public class DeploymentCtlApplication extends Application {

	private Set<Object> singleton;

	public DeploymentCtlApplication() {
		singleton = Collections.singleton(new JacksonJsonProvider());
	}

	@Override
	public Set<Object> getSingletons() {
		return singleton;
	}

}
