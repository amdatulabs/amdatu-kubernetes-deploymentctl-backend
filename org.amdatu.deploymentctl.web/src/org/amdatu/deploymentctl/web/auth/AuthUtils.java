package org.amdatu.deploymentctl.web.auth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class AuthUtils {

    private static final String ROLE_ADMIN = "admin";

    public static boolean isAdmin(WebUser webuser) {
        return hasRole(ROLE_ADMIN, webuser);
    }

    public static boolean hasRole(String role, WebUser webuser) {
        if (authDisabled()) {
            return true;
        }

        return getRoles(webuser).contains(role);
    }
    
    public static List<String> getRoles(WebUser webuser) {
        if (webuser != null) {
            JsonObject principal = getPrincipal(webuser);
            if (principal != null) {
            		JsonObject access = principal.getJsonObject("realm_access");
                if (access != null) {
                    JsonArray roles = access.getJsonArray("roles");
                    List<String> rolesList = new ArrayList<>();
                    roles.forEach(role -> rolesList.add(role.toString()));
                    return rolesList;
                }
            }
        }
        return Collections.emptyList();
    }

    public static String getUserName(WebUser webuser) {
        final JsonObject principal = getPrincipal(webuser);
        if (principal != null) {
            return principal.getString("sub");
        }
        return null;
    }
    
    private static JsonObject getPrincipal(Object webuser) {
    		if (webuser != null) {
    	        return ((WebUser) webuser).getPrincipal();
    		}
    		return null;
    }

    public static boolean authDisabled() {
        String value = System.getProperty("disableauth");
        if (value == null) {
            value = System.getenv("disableauth");
        }

        return Boolean.valueOf(value);
    }
}
