package org.amdatu.deploymentctl.web.auth;

import io.vertx.core.json.JsonObject;

public class WebUser {

	private final JsonObject m_principal;

	public WebUser(JsonObject principal) {
		this.m_principal = principal;
	}

	public JsonObject getPrincipal() {
		return m_principal;
	}

}
