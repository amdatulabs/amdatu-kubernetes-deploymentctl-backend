package org.amdatu.deploymentctl.web;

import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_NAME;
import static org.osgi.service.http.whiteboard.HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME;

public interface WebConstants {

	String CONTEXT_NAME = "org.amdatu.deploymentctl.web.auth";
	String CONTEXT_SELECT_FILTER = "(" + HTTP_WHITEBOARD_CONTEXT_NAME + "=" + CONTEXT_NAME + ")";

	String PUBLIC_CONTEXT_NAME = "org.amdatu.deploymentctl.web.public";
	String PUBLIC_CONTEXT_SELECT_FILTER = "(" + HTTP_WHITEBOARD_CONTEXT_NAME + "=" + PUBLIC_CONTEXT_NAME + ")";

	// REST
	String JAX_RS_APPLICATION_NAME = "org.amdatu.deploymentctl.web";
	String JAX_RS_APPLICATION_BASE = "/rest";

	// Websocket
	String WS_SERVLET_NAME = "org.amdatu.deploymentctl.web";
	String WS_SERVLET_SELECT_FILTER = "(" + WEBSOCKET_NAME + "=" + WS_SERVLET_NAME + ")";
}
