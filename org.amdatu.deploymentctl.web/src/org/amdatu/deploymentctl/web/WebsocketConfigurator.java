package org.amdatu.deploymentctl.web;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

public class WebsocketConfigurator extends ServerEndpointConfig.Configurator {

    @Override
    public void modifyHandshake(ServerEndpointConfig conf, HandshakeRequest req, HandshakeResponse resp) {

    		// get user's roles from HttpSession (see ServletContextHelper) and put them into user's properties
    		HttpSession httpSession = (HttpSession) req.getHttpSession();
    		@SuppressWarnings("unchecked")
    		List<String> roles = (List<String>) httpSession.getAttribute("roles");
    		conf.getUserProperties().put("roles", roles);
    		
    }
    
}
