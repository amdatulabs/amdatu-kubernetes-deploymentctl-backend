/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.amdatu.deploymentctl.dockerregistry.Organisation;
import org.amdatu.deploymentctl.dockerregistry.RegistryService;
import org.amdatu.deploymentctl.dockerregistry.RepoLoginService;
import org.amdatu.deploymentctl.redis.RedisService;
import org.amdatu.deploymentctl.web.WebConstants;
import org.amdatu.deploymentctl.web.auth.AuthUtils;
import org.amdatu.deploymentctl.web.auth.WebUser;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component(provides = Object.class)
@Property(name = AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME, value = WebConstants.JAX_RS_APPLICATION_NAME)
@Path("/registry")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RegistryResource {

	private final Logger logger = LoggerFactory.getLogger(RegistryResource.class);

	@ServiceDependency
	private volatile RepoLoginService repoLoginService;

	@ServiceDependency
	private volatile RegistryService registryService;

	@ServiceDependency
	private volatile RedisService redisService;

	private final ObjectMapper objectMapper = new ObjectMapper();

	@GET
	@Path("/repositories")
	public void listRepositories(
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response
	){

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		final String username = AuthUtils.getUserName(webuser);
		if (username == null) {
			response.resume(new ForbiddenException());
		}

		getDockerJwt(username)
			.flatMap(docker -> registryService.listRepos(docker.getString("username"), docker.getString("jwt")))
			.toList()
			.subscribe(response::resume, response::resume);
	}

	private Observable<JsonObject> getDockerJwt(String user) {
		return redisService.getClient().hgetallObservable("docker-jwt:" + user);
	}

	@POST
	@Path("/login")
	public void login(
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response,
			String body
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		final String username = AuthUtils.getUserName(webuser);
		if (username == null) {
			response.resume(new ForbiddenException());
		}

		final JsonNode jsonBody;
		try {
			jsonBody = objectMapper.reader().readTree(body);
		} catch (IOException e) {
			logger.error("error parsing login request", e);
			response.resume(e);
			return;
		}
		final String dockerUser = jsonBody.get("username").asText();
		final String dockerPwd = jsonBody.get("password").asText();

		repoLoginService.getJWT(dockerUser, dockerPwd)
			.flatMap(token -> storeToken(username, dockerUser, token))
			.map(token -> new JsonObject().put("token", token).encode())
			.subscribe(response::resume, response::resume);
	}

	@POST
	@Path("/logout")
	public void logout(
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		final String username = AuthUtils.getUserName(webuser);
		if (username == null) {
			response.resume(new ForbiddenException());
		}

		redisService.getClient().del("docker-jwt:" + username, null);
		response.resume(Response.ok().build());
	}

	@GET
	@Path("/{namespace}/{repo}/tags")
	public void listTagsHandler(
			@PathParam("namespace") String namespace,
			@PathParam("repo") String repo,
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response

	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		final String username = AuthUtils.getUserName(webuser);
		if (username == null) {
			response.resume(new ForbiddenException());
		}

		getDockerJwt(username)
			.flatMap(docker -> registryService.listTags(namespace + "/" + repo, docker.getString("jwt")))
			.toList()
			.map(Json::encode)
			.subscribe(response::resume, response::resume);
	}

	@GET
	@Path("/username")
	public void getUsername(
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		final String username = AuthUtils.getUserName(webuser);
		if (username == null) {
			response.resume(new ForbiddenException());
		}

		getDockerJwt(username)
			.flatMap(docker -> validateDockerToken(docker, username))
			.map(docker -> docker.getString("username"))
			.subscribe(response::resume, response::resume);
	}

	private Observable<JsonObject> validateDockerToken(JsonObject docker, String username) {
		if (docker != null && !docker.isEmpty()) {
			String token = docker.getString("jwt");
			// just try to list organisations in order to check if token is still valid
			return registryService.listOrganisations(token)
				.doOnError(err -> redisService.getClient().del("docker-jwt:" + username, null))
				.firstOrDefault(new Organisation())
				.map(something -> docker);
		}
		throw new RuntimeException("NO_DOCKER_TOKEN_FOUND");
	}

	private Observable<String> storeToken(String username, String dockerUsername, String token) {
		return redisService.getClient()
				.hmsetObservable("docker-jwt:" + username,
						new JsonObject()
						.put("jwt", token)
						.put("username", dockerUsername))
				.timeout(3, TimeUnit.SECONDS);
	}

}
