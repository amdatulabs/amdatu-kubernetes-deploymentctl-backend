/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TagInfo {
	@JsonProperty("name")
	private String name;
	@JsonProperty("full_size")
	private Long fullSize;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("repository")
	private Integer repository;
	@JsonProperty("creator")
	private Integer creator;
	@JsonProperty("last_updater")
	private Integer lastUpdater;
	@JsonProperty("last_updated")
	private String lastUpdated;
	@JsonProperty("image_id")
	private Object imageId;
	@JsonProperty("v2")
	private Boolean v2;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getFullSize() {
		return fullSize;
	}

	public void setFullSize(Long fullSize) {
		this.fullSize = fullSize;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRepository() {
		return repository;
	}

	public void setRepository(Integer repository) {
		this.repository = repository;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public Integer getLastUpdater() {
		return lastUpdater;
	}

	public void setLastUpdater(Integer lastUpdater) {
		this.lastUpdater = lastUpdater;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Object getImageId() {
		return imageId;
	}

	public void setImageId(Object imageId) {
		this.imageId = imageId;
	}

	public Boolean getV2() {
		return v2;
	}

	public void setV2(Boolean v2) {
		this.v2 = v2;
	}

	@Override
	public String toString() {
		return "TagInfo [name=" + name + ", repository=" + repository + "]";
	}

}
