/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry;

import rx.Observable;

public interface RepoLoginService {

	Observable<String> getJWT(String username, String password);

}