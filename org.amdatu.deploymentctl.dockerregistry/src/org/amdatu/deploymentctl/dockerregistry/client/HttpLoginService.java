/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry.client;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.amdatu.asynchttp.AsyncHttpClientService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.asynchttpclient.Response;

import org.amdatu.deploymentctl.dockerregistry.RepoLoginService;

import io.vertx.core.json.JsonObject;
import rx.Observable;

@Component
public class HttpLoginService implements RepoLoginService {
	@ServiceDependency
	private volatile AsyncHttpClientService m_httpClientService;

	@Override
	public Observable<String> getJWT(String username, String password) {
		JsonObject loginObject = new JsonObject().put("username", username).put("password", password);

		Map<String, Collection<String>> headers = new HashMap<>();
		headers.put("Content-Type", Arrays.asList("application/json"));

		return m_httpClientService.post(HttpRegistryService.HUB_BASE_URL + "users/login/", loginObject.encode(), headers)
				.map(Response::getResponseBody)
				.map(JsonObject::new)
				.map(json -> json.getString("token"))
				.map(token -> "JWT " + token);
	}
}
