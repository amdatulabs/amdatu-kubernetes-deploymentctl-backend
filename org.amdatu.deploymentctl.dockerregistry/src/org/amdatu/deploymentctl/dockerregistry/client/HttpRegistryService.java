/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry.client;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.amdatu.asynchttp.AsyncHttpClientService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.asynchttpclient.Response;

import org.amdatu.deploymentctl.dockerregistry.Organisation;
import org.amdatu.deploymentctl.dockerregistry.RegistryService;
import org.amdatu.deploymentctl.dockerregistry.Repo;
import org.amdatu.deploymentctl.dockerregistry.TagInfo;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import rx.Observable;

@Component
public class HttpRegistryService implements RegistryService {

	public final static String HUB_BASE_URL = "https://hub.docker.com/v2/";
	
	@ServiceDependency
	private volatile AsyncHttpClientService m_httpClientService;

	@Override
	public Observable<Repo> listRepos(String username, String jwt) {
		return listOrganisations(jwt)
				.map(o -> o.getOrgname())
				.mergeWith(Observable.just(username))
				.flatMap(org -> queryRepositories(org, jwt));		
	}
	
	@Override
	public Observable<Organisation> listOrganisations(String jwt) {
		return m_httpClientService.get(HUB_BASE_URL + "user/orgs/", getJwtAuthHeader(jwt))
				.flatMap(r -> mapJsonResult(r, Organisation.class));	
	}
	
	@Override
	public Observable<TagInfo> listTags(String repo, String jwt) {
		return m_httpClientService.get(HUB_BASE_URL + "repositories/" + repo + "/tags/", getJwtAuthHeader(jwt))
				.flatMap(r -> mapJsonResult(r, TagInfo.class));
	}
	
	private Observable<Repo> queryRepositories(String org, String jwt) {
		return m_httpClientService.get(HUB_BASE_URL + "repositories/" + org + "/?page_size=1000", getJwtAuthHeader(jwt))
				.flatMap(r -> mapJsonResult(r, Repo.class));
	}
	
	private Map<String, Collection<String>> getJwtAuthHeader(String jwt) {
		Map<String, Collection<String>> authHeaders = new HashMap<>();
		authHeaders.put("Authorization", Arrays.asList(jwt));
		
		return authHeaders;
	}
	
	private <T> Observable<T> mapJsonResult(Response response, Class<T> clazz) {
		String body = response.getResponseBody();
		JsonArray results = new JsonObject(body).getJsonArray("results");
		
		return Observable.from(results)
				.map(j -> j.toString())
				.map(s -> decodeJson(s, clazz));
	}
	
	private <T> T decodeJson(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			return mapper.readValue(json, clazz);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
