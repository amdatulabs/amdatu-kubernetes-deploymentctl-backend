/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Repo {
	private String user;
	private String name;
	private String namespace;
	private int status;
	private String description;
	private boolean isPrivate;
	private boolean isAutomated;
	private boolean canEdit;
	private int starCount;
	private int pullCount;
	private String lastUpdated;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public boolean isAutomated() {
		return isAutomated;
	}

	public void setAutomated(boolean isAutomated) {
		this.isAutomated = isAutomated;
	}

	public boolean isCanEdit() {
		return canEdit;
	}

	public void setCanEdit(boolean canEdit) {
		this.canEdit = canEdit;
	}

	public int getStarCount() {
		return starCount;
	}

	public void setStarCount(int starCount) {
		this.starCount = starCount;
	}

	public int getPullCount() {
		return pullCount;
	}

	public void setPullCount(int pullCount) {
		this.pullCount = pullCount;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((namespace == null) ? 0 : namespace.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Repo other = (Repo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Repo [user=" + user + ", name=" + name + ", namespace=" + namespace + ", status=" + status
				+ ", description=" + description + ", isPrivate=" + isPrivate + ", isAutomated=" + isAutomated
				+ ", canEdit=" + canEdit + ", starCount=" + starCount + ", pullCount=" + pullCount + ", lastUpdated="
				+ lastUpdated + "]";
	}
}
