/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry;

import rx.Observable;

public interface RegistryService {
	Observable<Organisation> listOrganisations(String jwt);
	Observable<Repo> listRepos(String username, String jwt);
	Observable<TagInfo> listTags(String repo, String jwt);
}