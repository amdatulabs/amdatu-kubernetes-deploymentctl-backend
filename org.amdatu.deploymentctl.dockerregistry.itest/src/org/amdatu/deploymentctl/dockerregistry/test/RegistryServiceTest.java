/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.dockerregistry.test;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.amdatu.deploymentctl.dockerregistry.RegistryService;
import org.amdatu.deploymentctl.dockerregistry.Repo;
import org.amdatu.deploymentctl.dockerregistry.RepoLoginService;

import io.vertx.rxjava.core.Vertx;
import rx.observers.TestSubscriber;

public class RegistryServiceTest {

	private volatile RegistryService m_registryService;
	private volatile RepoLoginService m_loginService;

	@Before
	public void setup() {
		configure(this)
				.add(createServiceDependency().setService(RegistryService.class).setRequired(true))
				.add(createServiceDependency().setService(RepoLoginService.class).setRequired(true))
				.add(createComponent().setInterface(Vertx.class.getName(), null).setImplementation(Vertx.vertx()))
				.apply();
	}

	@Test
	public void test() {

		TestSubscriber<Repo> testSubscriber = new TestSubscriber<>();

		m_loginService.getJWT("amdatuci", "WDgGd6MzfFpBFf9j")
			.flatMap(jwt -> m_registryService.listRepos("amdatuci", jwt))
			.subscribe(testSubscriber);
		
		testSubscriber.awaitTerminalEvent();
		testSubscriber.getOnErrorEvents().forEach(e -> e.printStackTrace());
		testSubscriber.assertNoErrors();
		List<Repo> onNextEvents = testSubscriber.getOnNextEvents();
		assertTrue("Should have more than 1 repo", onNextEvents.size() > 1);
	}

	@After
	public void after() {
		cleanUp(this);
	}
}