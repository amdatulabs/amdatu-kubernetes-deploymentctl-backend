/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.deploy;

public class DockerImage {
	
	private String m_namespace;
	private String m_name;
	private String m_tag;
	private String m_callbackUrl;
	
	public DockerImage(String image) {
		String[] parts = image.split(":");
		
		if (parts.length != 2) {
			throw new IllegalArgumentException("cannot parse image");
		}
		
		String tmpImage = parts[0];
		m_tag = parts[1];
		
		parts = tmpImage.split("/");
		if (parts.length == 1) {
			m_namespace = "library";
			m_name = tmpImage;
		} else if (parts.length == 2) {
			m_namespace = parts[0];
			m_name = parts[1];
		} else {
			throw new IllegalArgumentException("cannot parse image");
		}
	}
	
	public DockerImage(String namespace, String name, String tag, String callbackUrl) {
		super();
		m_namespace = namespace;
		m_name = name;
		m_tag = tag;
		m_callbackUrl = callbackUrl;
	}

	public String getNamespace() {
		return m_namespace;
	}
	
	public void setNamespace(String namespace) {
		m_namespace = namespace;
	}
	
	public String getName() {
		return m_name;
	}
	
	public void setName(String name) {
		m_name = name;
	}
	
	public String getTag() {
		return m_tag;
	}
	
	public void setTag(String tag) {
		m_tag = tag;
	}

	public String getCallbackUrl() {
		return m_callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		m_callbackUrl = callbackUrl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_name == null) ? 0 : m_name.hashCode());
		result = prime * result + ((m_namespace == null) ? 0 : m_namespace.hashCode());
		result = prime * result + ((m_tag == null) ? 0 : m_tag.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DockerImage other = (DockerImage) obj;
		if (m_name == null) {
			if (other.m_name != null)
				return false;
		} else if (!m_name.equals(other.m_name))
			return false;
		if (m_namespace == null) {
			if (other.m_namespace != null)
				return false;
		} else if (!m_namespace.equals(other.m_namespace))
			return false;
		if (m_tag == null) {
			if (other.m_tag != null)
				return false;
		} else if (!m_tag.equals(other.m_tag))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DockerImage [m_namespace=" + m_namespace + ", m_name=" + m_name + ", m_tag=" + m_tag
				+ ", m_callbackUrl=" + m_callbackUrl + "]";
	}
	
}
