/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.deploy;

import java.util.List;

import io.fabric8.kubernetes.api.model.PodSpec;

public class Descriptor {

	private List<WebHook> m_webhooks;
	private String m_namespace;
	private String m_appName;
	private PodSpec m_podspec;

	public List<WebHook> getWebhooks() {
		return m_webhooks;
	}

	public void setWebhooks(List<WebHook> webhooks) {
		m_webhooks = webhooks;
	}

	public String getNamespace() {
		return m_namespace;
	}

	public void setNamespace(String namespace) {
		m_namespace = namespace;
	}

	public String getAppName() {
		return m_appName;
	}

	public void setAppName(String appName) {
		m_appName = appName;
	}

	public PodSpec getPodspec() {
		return m_podspec;
	}

	public void setPodspec(PodSpec podspec) {
		m_podspec = podspec;
	}

	@Override
	public String toString() {
		return "Deployment [m_webhooks=" + m_webhooks + ", m_namespace=" + m_namespace + ", m_appName="
				+ m_appName + ", m_podspec=" + m_podspec + "]";
	}


}
