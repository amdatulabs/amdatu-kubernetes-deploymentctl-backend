/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.deploy;

public class WebHook {

	private String m_key;

		public String getKey() {
		return m_key;
	}

	public void setKey(String key) {
		m_key = key;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((m_key == null) ? 0 : m_key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebHook other = (WebHook) obj;
		if (m_key == null) {
			if (other.m_key != null)
				return false;
		} else if (!m_key.equals(other.m_key))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WebHook [m_key=" + m_key + "]";
	}
	
}
