/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.deploy;

public class Deployment {

	private String m_id;
	private String m_lastModified;
	private String m_status;
	private Descriptor m_descriptor;

	public String getId() {
		return m_id;
	}

	public void setId(String id) {
		m_id = id;
	}
	
	public String getLastModified() {
		return m_lastModified;
	}
	
	public void setLastModified(String lastModified) {
		m_lastModified = lastModified;
	}
	public String getStatus() {
		return m_status;
	}

	public void setStatus(String status) {
		m_status = status;
	}

	public Descriptor getDescriptor() {
		return m_descriptor;
	}
	
	public void setDescriptor(Descriptor descriptor) {
		this.m_descriptor = descriptor;
	}

	@Override
	public String toString() {
		return "Deployment [m_id=" + m_id + ", m_lastModified=" + m_lastModified + ", m_descriptor=" + m_descriptor
				+ "]";
	}
	
}
