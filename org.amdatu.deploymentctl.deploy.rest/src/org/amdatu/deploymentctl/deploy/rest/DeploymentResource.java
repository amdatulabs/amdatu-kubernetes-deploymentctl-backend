/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.deploy.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.amdatu.asynchttp.AsyncHttpClientService;
import org.amdatu.deploymentctl.configuration.Config;
import org.amdatu.deploymentctl.deploy.Deployment;
import org.amdatu.deploymentctl.deploy.Descriptor;
import org.amdatu.deploymentctl.deploy.DockerImage;
import org.amdatu.deploymentctl.deploy.WebHook;
import org.amdatu.deploymentctl.events.EventsConstants;
import org.amdatu.deploymentctl.web.WebConstants;
import org.amdatu.deploymentctl.web.auth.AuthUtils;
import org.amdatu.deploymentctl.web.auth.WebUser;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.http.HttpClient;
import rx.Observable;

@Component(provides = Object.class)
@Property(name = AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME, value = WebConstants.JAX_RS_APPLICATION_NAME)
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeploymentResource {

	private final Logger logger = LoggerFactory.getLogger(DeploymentResource.class);

	@ServiceDependency
	private volatile Vertx vertx;

	@ServiceDependency
	private volatile AsyncHttpClientService asyncClient;

	@ServiceDependency
	private volatile Config config;

	@ServiceDependency
	private volatile EventAdmin eventAdmin;

	@ServiceDependency
	private volatile ClientBuilder clientBuilder;

	private final ObjectMapper objectMapper = new ObjectMapper();

	private volatile HttpClient httpClient;

	public DeploymentResource() {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@Start
	public void start() {

		httpClient = vertx.createHttpClient(new HttpClientOptions());

	}

	@GET
	@Path("/stream/deployments/{id}/logs")
	public void streamLogs(
			@PathParam("id") String deploymentId,
			@QueryParam("topicId") String topicId,
			@QueryParam("namespace") String namespace,
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		if (!AuthUtils.hasRole(namespace, webuser)) {
			response.resume(new ForbiddenException());
			return;
		}

		streamLogs(namespace, deploymentId).subscribe(
				log -> {
					logger.debug("streaming logs to " + topicId + "...\n");// + l);
					eventAdmin.postEvent(createLogEvent(topicId, log));
				},
				err -> {
					logger.error("error streaming logs: " + err.getMessage());
					eventAdmin.postEvent(createLogEvent(topicId, "Error streaming logs: " + err.getMessage()));
				},
				() -> {
					logger.debug("finished streaming logs sucessfully!");
					eventAdmin.postEvent(createLogEvent(topicId, "!!completed"));
				}
		);

		response.resume(Response.ok().build());
	}

	private Observable<String> streamLogs(final String namespace, final String id) {
		return Observable.create(observer -> {
			String path = String.format("/stream/deployments/%s/logs?namespace=%s", id, namespace);
			logger.debug("openening websocket connection to " + path);
			httpClient.websocket(config.deployerPort, config.deployerHost, path,
					websocket -> {
						logger.debug("Websocket connection with deployer opened");

						websocket.handler(buffer -> {
							observer.onNext(buffer.toString());
						});

						websocket.closeHandler(void_ -> {
							logger.debug("Websocket connection with deployer closed");
							observer.onCompleted();
						});

					},
					throwable -> {
						observer.onError(throwable);
					}
			);
		});
	}

	private Event createLogEvent(String topicId, String msg) {
		Dictionary<String, Object> properties = new Hashtable<>();
		properties.put(EventsConstants.PROPERTY_PAYLOAD, msg);
		return new Event(EventsConstants.TOPIC_LOGS + topicId, properties);
	}


	@GET
	@Path("{path:(descriptors|deployments).*}")
	public void proxyGet(
			@QueryParam("namespace") String namespace,
			@Context HttpServletRequest servletRequest,
			@Suspended AsyncResponse asyncResponse
	) {

		proxy(namespace, servletRequest, asyncResponse, null);

	}

	@POST
	@Path("{path:(descriptors|deployments).*}")
	public void proxyPost(
			@QueryParam("namespace") String namespace,
			@Context HttpServletRequest servletRequest,
			@Suspended AsyncResponse asyncResponse,
			String body
	) {

		proxy(namespace, servletRequest, asyncResponse, body);

	}

	@PUT
	@Path("{path:(descriptors|deployments).*}")
	public void proxyPut(
			@QueryParam("namespace") String namespace,
			@Context HttpServletRequest servletRequest,
			@Suspended AsyncResponse asyncResponse,
			String body
	) {

		proxy(namespace, servletRequest, asyncResponse, body);

	}

	@DELETE
	@Path("{path:(descriptors|deployments).*}")
	public void proxyDelete(
			@QueryParam("namespace") String namespace,
			@Context HttpServletRequest servletRequest,
			@Suspended AsyncResponse asyncResponse
	) {

		proxy(namespace, servletRequest, asyncResponse, null);

	}

	private void proxy(String namespace, HttpServletRequest servletRequest, AsyncResponse asyncResponse, String body) {

		if (!servletRequest.getMethod().equalsIgnoreCase("OPTIONS")) {
			WebUser webuser = (WebUser)servletRequest.getAttribute(WebUser.class.getName());
			if (!AuthUtils.hasRole(namespace, webuser)) {
				asyncResponse.resume(new ForbiddenException());
				return;
			}
		}

		String path = servletRequest.getRequestURI()
				.substring(servletRequest.getContextPath().length() + WebConstants.JAX_RS_APPLICATION_BASE.length());

		final String target = String.format("http://%s:%s%s/?%s", config.deployerHost, config.deployerPort, path, servletRequest.getQueryString());
		logger.debug("Proxying request: " + target);

		// send request to deployer
		final Invocation.Builder builder = clientBuilder.build().target(target).request();
		Collections.list(servletRequest.getHeaderNames()).stream()
				.filter(headerName -> !headerName.equalsIgnoreCase("Content-Length"))
				.forEach(headerName -> builder.header(headerName, servletRequest.getHeader(headerName)));
		final Invocation invocation;
		if (body != null && body.length() > 0) {
			invocation = builder.build(servletRequest.getMethod(), Entity.json(body));
		} else {
			invocation = builder.build(servletRequest.getMethod());
		}
		final Response clientResponse = invocation.invoke();
		clientResponse.bufferEntity();

		// create response from deployer response
		final Response.ResponseBuilder responseBuilder = Response.status(clientResponse.getStatus());
		clientResponse.getHeaders().keySet().stream().forEach(headerName -> responseBuilder.header(headerName, clientResponse.getHeaderString(headerName)));
		responseBuilder.entity(clientResponse.readEntity(String.class));
		clientResponse.close();
		asyncResponse.resume(responseBuilder.build());

	}

	@POST
	@Path("/webhooks/{namespace}/{webhook}")
	public void webhookHandler(
			@PathParam("namespace") String namespace,
			@PathParam("webhook") String webhookKey,
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response,
			String body
	) {
		final JsonNode jsonNode;
		try {
			jsonNode = objectMapper.reader().readTree(body);
		} catch (IOException e) {
			logger.error("error parsing webhook request", e);
			response.resume(e);
			return;
		}
		final DockerImage webhookImage = getWebhookImage(jsonNode);
		final String callbackUrl = webhookImage != null ? webhookImage.getCallbackUrl() : null;

		logger.info(String.format("webhook called, namespace: %s, key: %s, image: %s", namespace, webhookKey, webhookImage != null ? webhookImage.toString() : "unknown"));

		final WebHook webhook = new WebHook();
		webhook.setKey(webhookKey);

		final List<String> redeploymentLocations = new ArrayList<>();

		getDeploymentHistory(namespace)
			.flatMap(this::jsonArrayToDeployments)
			.filter(d -> isMatchingDeployment(d, namespace, webhook, webhookImage))
			.toSortedList( (d1, d2) -> d2.getLastModified().compareTo(d1.getLastModified()) )
			.map(l -> {
				if (l.size() > 0) {
					return l.get(0);
				} else {
					throw new RuntimeException("No matching deployment found");
				}
			})
			.flatMap(d -> this.redeploy(namespace, d.getId()))
			.subscribe(
					location -> redeploymentLocations.add(location),
					err -> {
						logger.error("error triggering redeployments");
						response.resume(err);
						callCallback(callbackUrl, false, err.getMessage());
					},
					() -> {
						response.resume(Response.status(202).entity("Started deployment"));
						checkDeployments(redeploymentLocations, callbackUrl);
					}
			);

	}

	private void checkDeployments(List<String> redeploymentLocations, String callbackUrl) {

		callCallback(callbackUrl, false, "Deployment started, wait for update!");

		int time = 600; // wait at most 10 minutes
		int sleepTime = 15; // seconds
		final AtomicBoolean failed = new AtomicBoolean(false);

		logger.info("Checking deployments");

		while (!failed.get() && time > 0 && redeploymentLocations.size() > 0) {
			final List<String> checkedLocations = new ArrayList<>();
			try {
				for (String location : redeploymentLocations) {
					String url = "http://" + config.deployerHost + ":" + config.deployerPort + location;
					logger.info("Checking " + url);
					httpClient.getAbs(url)
						.handler(response -> {
							response.bodyHandler(buffer -> {
								logger.info("  Parsing response body");
								String status = buffer.toJsonObject().getString("status");
								logger.info("    Status: " + status);
								if (status.equals("DEPLOYED")) {
									checkedLocations.add(location);
								} else if (status.equals("FAILURE")) {
									failed.set(true);
								}
							});
						}).end();
				}

				Thread.sleep(sleepTime * 1000);

			} catch (Throwable e) {
				logger.error("Error checking deployments: " + e.getMessage());
				failed.set(true);
			}

			time -= sleepTime;
			redeploymentLocations.removeAll(checkedLocations);
		}

		if (failed.get()) {
			logger.info("Deployment failed!");
			callCallback(callbackUrl, false, "Deployment failed!");
		} else if (redeploymentLocations.size() > 0) {
			logger.info("Deployment timed out!");
			callCallback(callbackUrl, false, "Deployment timed out!");
		} else {
			logger.info("Deployment successful!");
			callCallback(callbackUrl, true, "Deployment successful!");
		}

	}

	private void callCallback(String callbackUrl, boolean success, String message) {
		if (callbackUrl == null) {
			return;
		}
		logger.info("Calling webhook callback: " + callbackUrl);

		String body = new JsonObject()
			.put("state", success ? "success" : "error")
			.put("description", message)
			.put("context", "CloudRTI Webhooks")
			.encode();

		logger.info("  body: " + body);

		httpClient.postAbs(callbackUrl, response -> {
			logger.info("  Webhook callback response: " + response.statusMessage());
		}).exceptionHandler(
				e -> logger.error("Error calling docker hub callback", e)
		).putHeader("Content-Type", "application/json").end(body);
	}

	private Observable<String> getDeploymentHistory(String namespace) {
		return asyncClient.get("http://" + config.deployerHost + ":" + config.deployerPort + "/deployments/?namespace=" + namespace).map(resp -> resp.getResponseBody());
	}

	protected Observable<Deployment> jsonArrayToDeployments(String json) {
		List<Deployment> deployments;
		try {
			deployments = objectMapper.readValue(json, new TypeReference<List<Deployment>>(){});
			return Observable.from(deployments);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	protected boolean isMatchingDeployment(Deployment deployment, String namespace, WebHook webhook, DockerImage webhookImage) {
		Descriptor descriptor = deployment.getDescriptor();
		if (descriptor.getNamespace().equals(namespace)
				&& deployment.getStatus().equalsIgnoreCase("deployed")
				&& descriptor.getWebhooks() != null
				&& descriptor.getWebhooks().contains(webhook)) {
			logger.info(String.format("webhook found in deployment id %s, app %s, modified %s", deployment.getId(), descriptor.getAppName(), deployment.getLastModified()));
			if (webhookImage == null) {
				// if we don't know the pushed image, always redeploy
				logger.info("unknown image in webhook request, candidate for redeploying...");
				return true;
			}
			// check if the image of the webhook request is part of the deployment
			boolean imageFound = descriptor.getPodspec().getContainers().stream()
				.map(c -> new DockerImage(c.getImage()))
				.anyMatch(d -> d.equals(webhookImage));

			if (imageFound) {
				logger.info(String.format("candidate for redeploying, image matches: %s", webhookImage));
			} else {
				logger.info(String.format("no candidate for redeployment, no matching image found for %s", webhookImage));
			}

			return imageFound;
		}
		return false;
	}

	private Observable<String> redeploy(String namespace, String id) {
		logger.info(String.format("redeploying %s %s", namespace, id));
		String path = String.format("/deployments/%s/?namespace=%s", id, namespace);
//		logger.debug("  path: " + path);
		return asyncClient.put("http://" + config.deployerHost + ":" + config.deployerPort + path, null)
				.map(response -> response.getHeader("Location"));
	}

	private DockerImage getWebhookImage(JsonNode webhookRequestBody) {

		/*
		{
		  "push_data": {
		    "pushed_at": 1461758657,
		    "images": {},
		    "tag": "<tag>",
		    "pusher": "<pusher>"
		  },
		  "callback_url": "https://registry.hub.docker.com/u/<namespace>/<image>/hook/<callbackid>/",
		  "repository": {
		    "status": "Active",
		    "description": "",
		    "is_trusted": true,
		    "full_description": "...",
		    "repo_url": "https://hub.docker.com/r/<namespace>/<image>",
		    "owner": "<owner>",
		    "is_official": false,
		    "is_private": false,
		    "name": "<image>",
		    "namespace": "<namespace>",
		    "star_count": 0,
		    "comment_count": 0,
		    "date_created": 1434470540,
		    "dockerfile": "...",
		    "repo_name": "<namespace>/<image>"
		  }
		}
		*/

		JsonNode pushData = webhookRequestBody.get("push_data");
		String tag = pushData != null ? pushData.get("tag").asText() : null;
		JsonNode respository = webhookRequestBody.get("repository");
		if (respository != null) {
			String namespace = respository.get("namespace").asText();
			String imagename = respository.get("name").asText();
			if (tag != null && namespace != null && imagename != null) {
				String callback = webhookRequestBody.get("callback_url").asText();
				return new DockerImage(namespace, imagename, tag, callback);
			}
		}
		return null;
	}

	@Stop
	public void stop() {
		httpClient.close();
	}

}
