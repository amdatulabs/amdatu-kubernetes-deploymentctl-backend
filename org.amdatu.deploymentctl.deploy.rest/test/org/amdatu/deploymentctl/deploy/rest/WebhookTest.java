/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.deploy.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.amdatu.deploymentctl.deploy.DockerImage;
import org.amdatu.deploymentctl.deploy.WebHook;
import org.junit.Test;

import rx.Observable;

public class WebhookTest {

	@Test
	public void testFindDeployment() throws IOException {

		DeploymentResource dr = new DeploymentResource();

		String json = new String(Files.readAllBytes(Paths.get("test/org/amdatu/deploymentctl/deploy/rest/deployments.json")), Charset.forName("UTF-8"));

		DockerImage dockerImage = new DockerImage("somename", "someImage", "v1", null);
		WebHook webHook = new WebHook();
		webHook.setKey("f0c7cbc7-6ca6-f414-1790-e44fda4c5c2a");

		Observable.just(json)
			.flatMap(dr::jsonArrayToDeployments)
			.filter(d -> dr.isMatchingDeployment(d, "testns", webHook, dockerImage))
			.toSortedList( (d1, d2) -> d2.getLastModified().compareTo(d1.getLastModified()) )
			.map(l -> {
				if (l.size() > 0) {
					return l.get(0);
				} else {
					throw new RuntimeException("no matching deployment found");
				}
			})
			.subscribe(
					d -> {
						assertEquals("wrong deployment found!", "2016-07-11T13:15:02Z", d.getLastModified());
						assertEquals("wrong deployment found!", "testns", d.getDescriptor().getNamespace());
						assertEquals("wrong deployment found!", "app1", d.getDescriptor().getAppName());
						assertEquals("wrong deployment found!", "f0c7cbc7-6ca6-f414-1790-e44fda4c5c2a", d.getDescriptor().getWebhooks().get(0).getKey());
					},
					e -> {
						System.out.println("Error! " + e.getMessage());
						e.printStackTrace();
						fail();
					},
					() -> System.out.println("done")
			);

	}
}
