package org.amdatu.deploymentctl.events.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import org.amdatu.deploymentctl.events.EventsConstants;
import org.amdatu.deploymentctl.web.WebsocketConfigurator;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.Start;
import org.apache.felix.dm.annotation.api.Stop;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

import static org.amdatu.deploymentctl.web.WebConstants.WS_SERVLET_SELECT_FILTER;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_ENDPOINT;
import static org.amdatu.web.websocket.WebSocketConstants.WEBSOCKET_SERVLET_SELECT;

@ServerEndpoint(value = "/events", configurator = WebsocketConfigurator.class)
@Property(name = WEBSOCKET_ENDPOINT, value = "true")
@Property(name = WEBSOCKET_SERVLET_SELECT, value = WS_SERVLET_SELECT_FILTER)
@Property(name = EventConstants.EVENT_TOPIC, value = EventsConstants.TOPIC_WEB + "*")
@Component(provides = {EventsEndpoint.class, EventHandler.class})
public class EventsEndpoint implements EventHandler {

	private static final String COMMAND_REGISTER_TOPIC = "registerTopic";

	private final Logger logger = LoggerFactory.getLogger(EventsEndpoint.class);
	
	private final Map<String, Client> clients = new ConcurrentHashMap<>();
	private final ObjectMapper mapper = new ObjectMapper();
	
	private ExecutorService workerExecuter;
	private ScheduledExecutorService metricsExecuter;
	
	private static final Gauge queueSizes = Gauge.build()
		     .name("deploymentctl_events_queue_size")
		     .labelNames("client_id")
		     .help("Size of the events message queue.")
		     .register();

	private static final Counter queueProcessed = Counter.build()
		     .name("deploymentctl_events_queue_processed_total")
		     .labelNames("client_id")
		     .help("Nr of the processed events.")
		     .register();

	private static final Gauge threadPoolSize = Gauge.build()
		     .name("deploymentctl_events_worker_threadpool_active")
		     .help("Nr of active threads in the events worker thread pool.")
		     .register();
	
	private static final Counter nrIncomingEvents = Counter.build()
		     .name("deploymentctl_events_incoming_total")
		     .help("Nr of incoming events.")
		     .register();

	@Start
	private void start() {
		
		ThreadGroup workerThreadGroup = new ThreadGroup("eventsWorkerThreadPool");
		workerExecuter = Executors.newCachedThreadPool(r -> new Thread(workerThreadGroup, r, "eventsWorkerThread"));
		
		metricsExecuter = Executors.newSingleThreadScheduledExecutor(r -> new Thread(r, "eventsMetricsThread"));
		metricsExecuter.scheduleAtFixedRate(() -> {
			threadPoolSize.set(workerThreadGroup.activeCount());
		}, 0, 10, TimeUnit.SECONDS);
		
	}
	
	@Stop
	private void stop() {
		workerExecuter.shutdownNow();
		metricsExecuter.shutdown();
	}
	
	@OnMessage
    public void handleMessage(Session session, String message) {
        logger.debug("reveived message for id {}: {}", session.getId(), message);
        try {
        		Command cmd = mapper.readValue(message, Command.class);
        		
        		switch (cmd.getAction()) {
        		case COMMAND_REGISTER_TOPIC:
        			Client client = clients.get(session.getId());
        			if (client != null) {
        				client.addTopic(cmd.getValue());
        			} else {
        				logger.error("client with id {} not found!", session.getId());
        			}
        			break;
        		default:
        			logger.error("unknown action {}", cmd.getAction());
        		}
        } catch (Exception e) {
        		logger.error("couldn't parse msg: {}", message);
        }
    }

    @OnMessage
    public void handleBinaryMessage(Session session, byte[] message) {
        logger.warn("ignoring binary message for id {} ", session.getId());
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) throws IOException {
        logger.debug("opened connection for id {}  ", session.getId());
        
        @SuppressWarnings("unchecked")
		List<String> roles = (List<String>) session.getUserProperties().get("roles");
        clients.put(session.getId(), new Client(session, roles, workerExecuter, mapper, queueSizes, queueProcessed));
    }

    @OnClose
    public void onClose(Session session) {
        logger.debug("closed connection for id {} ", session.getId());
        removeClient(clients.get(session.getId()));
    }

    @OnError
    public void onError(Session session, Throwable e) {
        logger.warn("error on connection for id {} ", session.getId());    	
        removeClient(clients.get(session.getId()));
    }

	@Override
	public void handleEvent(Event event) {
		nrIncomingEvents.inc();
		clients.values().forEach(c -> {
			if (c.isClosed()) {
				removeClient(c);
			} else {
				c.queueEvent(event);					
			}
		});
	}
	
	private void removeClient(Client client) {
		client.close();
		clients.remove(client.getId());
		queueSizes.remove(client.getId());
		queueProcessed.remove(client.getId());
	}

}
