package org.amdatu.deploymentctl.events.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import org.amdatu.deploymentctl.events.EventsConstants;
import org.osgi.service.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

public class Client {
	
	private Logger logger = LoggerFactory.getLogger(Client.class);

	private String id;
	private Session session;
	private List<String> roles;
	private volatile Set<String> topics = Collections.newSetFromMap(new ConcurrentHashMap<>());

	private BlockingQueue<Event> eventQueue;
	private Gauge queueSizes;
	private Counter queueProcessed;
	private ExecutorService executer;
	private ObjectMapper mapper;
	
	private volatile boolean isClosed = false;
	private volatile boolean isSending = false;
	private final Object lock = new Object();
	
	private volatile int errorCount;
	
	public Client(Session session, List<String> roles, ExecutorService executer, ObjectMapper mapper, Gauge queueSizes, Counter queueProcessed) {
		this.session = session;
		this.id = session.getId();
		this.roles = roles;
		this.executer = executer;
		this.mapper = mapper;
		this.queueSizes = queueSizes;
		this.queueProcessed = queueProcessed;
		
		this.queueSizes.labels(id).set(0);
		this.eventQueue = new LinkedBlockingQueue<>();
	}
	
	public void queueEvent(Event event) {
		
		if (isClosed) {
			return;
		}
		
		// filter topic
		String topic = event.getTopic();
		if (!topics.contains(topic)) {
			return;
		}
		
		try {
			eventQueue.put(event);			
			queueSizes.labels(id).inc();
		} catch (Exception e) {
			logger.error("error queueing event", e);
		}
	
		synchronized (lock) {
			if (!isSending) {
				try {
					isSending = true;
					executer.execute(this::processEvents);
				} catch (RuntimeException e) {
					isSending = false;
					logger.error("error executing next message polling", e);
					return;
				}				
			}
		}

	}
	
	private void processEvents() {

		try {

			while (!isClosed) {
				Event event;
				try {
					event = eventQueue.poll(100, TimeUnit.MILLISECONDS);
				} catch (Exception e) {
					logger.error("error polling message from queue", e);
					return;
				}
				if (event == null) {
					// no events available anymore
					return;
				}
				processEvent(event);
			}
					
		} finally {
			synchronized (lock) {
				isSending = false;				
			}
		}
			
	}
	
	private void processEvent(Event event) {

		// create message from event
		Object payload = event.getProperty(EventsConstants.PROPERTY_PAYLOAD);
		String msgPayload = "";
		
		if (payload instanceof String) {
			msgPayload = (String) payload;
		} else {
			logger.error("unsupported event payload type! {}" + payload.getClass().getName());
			return;
		}
		
		Message msg = new Message(event.getTopic(), msgPayload);
		String json;
		try {
			json = mapper.writeValueAsString(msg);
		} catch (JsonProcessingException e) {
			logger.error("error encoding message", e);
			return;
		}

		queueSizes.labels(this.getId()).dec();

		// send message
		// do it synchronously, so we keep messages in order
		// but we need to use AsyncRemote because BasicRemote doesn't work with multiple threads
		try {
			session.getAsyncRemote().sendText(json).get(5, TimeUnit.SECONDS);
			queueProcessed.labels(id).inc();
		} catch (Exception e) {
			logger.error("error sending event to client with id {}, message {}, error: {}", id, json, e.getClass().getName() + ": " +  e.getMessage());
			errorCount++;
			if (errorCount >= 3) {
				logger.error("too many errors, closing client with id {}", id);
				isClosed = true;
			}
		}
		
	}

	public void close() {
		isClosed = true;
		eventQueue.clear();
		try {
			session.close();
		} catch (IOException e) {
			// nothing we can do, and at the end not interesting
		}
	}
	
	public boolean isClosed() {
		return isClosed;
	}
	
	public String getId() {
		return id;
	}
		
	public void addTopic(String topic) {
		if (hasAccessToTopic(topic)) {
			topics.add(topic);			
		} else {
			logger.error("denying access to topic! {}", topic);
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Client [id=" + id + ", session=" + session + ", topics=" + topics + "]";
	}
	
	private boolean hasAccessToTopic(String topic) {
		
		if (topic.startsWith(EventsConstants.TOPIC_LOGS)) {
			// these need no namespace check
			return true;
		} else {
			logger.error("unknown topic, denying access! {}", topic);
			return false;
		}
		
	}

}
