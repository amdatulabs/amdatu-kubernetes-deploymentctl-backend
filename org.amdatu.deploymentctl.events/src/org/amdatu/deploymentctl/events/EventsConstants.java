package org.amdatu.deploymentctl.events;

public interface EventsConstants {
	
	String TOPIC_BASE = "org/amdatu/dctl/";
	String TOPIC_WEB = TOPIC_BASE + "web/";
	String TOPIC_LOGS = TOPIC_WEB + "logs/"; // identifier will be appended

	String PROPERTY_PAYLOAD = "payload";

}
