Introduction
===
Amdatu Kubernetes Deploymentctl provides basically a UI for the Amdatu Kubernetes Deployer, but it has some additional features.

This repository contains the backend part, see the Amdatu Kubernetes Deploymentctl Frontend repository for the frontend.

Features are:

* Integration with Keycloak for authentication and namespace based authorization
* Management of Kubernetes Secrets
* Docker Hub integration for easy selecting of Docker images and tags for deployments
* Webhooks for automated redeployments

Amdatu Kubernetes Deployer is used in several production environments, and is actively maintained.

Related components
===

Amdatu Kubernetes Deploymentctl depends on

* Kubernetes
* Amdatu Kubernetes Deployer
* Keycloak as auth backend
* A Redis data store

Getting started
===

For testing run the backend together with the frontend with Docker Compose. You can find a compose file in the `run` directory,
just change the environment variables to your needs.

In production we recommend to deploy DeploymentCtl to the Kubernetes cluster. This is an example spec:

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: deploymentctl
  name: deploymentctl
  namespace: kube-system
spec:
  replicas: 1
  selector:
    matchLabels:
      app: deploymentctl
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: deploymentctl
    spec:
      containers:
      - env:
        - name: deployer_host
          value: deployer
        - name: deployer_port
          value: "8000"
        - name: kubernetes_url
          value: http://<K8sMaster>:8080
        - name: redis_host
          value: redis
        - name: port
          value: "8585"
        - name: key_cloak_public_key
          value: MII.....
        image: amdatu/amdatu-kubernetes-deploymentctl-backend:production
        imagePullPolicy: Always
        name: backend
        ports:
        - containerPort: 8585
          name: backend
          protocol: TCP
        resources:
          requests:
            cpu: 250m
            memory: 512Mi
        volumeMounts:
        - mountPath: /app/webroot
          name: wwww
          readOnly: true
      - image: amdatu/amdatu-kubernetes-deploymentctl-frontend:production
        imagePullPolicy: Always
        lifecycle:
          postStart:
            exec:
              command:
              - cp
              - -R
              - /www/dist/.
              - /data
        name: frontend
        resources:
          requests:
            cpu: 50m
            memory: 64Mi
        volumeMounts:
        - mountPath: /data
          name: wwww
        - mountPath: /www/dist/keycloak
          name: keycloaksecret
          readOnly: true
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      volumes:
      - emptyDir: {}
        name: wwww
      - name: keycloaksecret
        secret:
          defaultMode: 420
          secretName: keycloak
```

Authentication and authorization
===
Authentication against the Kubernetes API is supported by the underlying Amdatu Kubernetes Java client, but not implemented in this component yet.

The frontend uses Keycloak for getting Java Web Tokens (JWT) with information about the user and namespaces the user has access to. These JWT are
verified at the backend.

Getting involved
===
[Bug reports and feature requests](https://amdatu.atlassian.net/projects/AKDCTL) and of course pull requests are greatly appreciated!
The project is built on [Bamboo](https://amdatu.atlassian.net/builds/browse/AKDCTLB-MAIN/latestSuccessful).
The build produces a runnable jar file and pushes a Docker image.

Versioning
===

The Docker images are tagged using a alpha/beta/prod schema.
The continuous build pushes `amdatu/deploymentctl-backend:alpha`.
When a version is stable, we promote it to `beta`.
The image is not rebuild, but a tag is set in git specifying the version number (e.g. 1.x.x).
For Docker we set multiple tags:

* beta
* 1.x.x

This way you can both reference the latest beta version, or a specific tagged version.
From beta we promote images to `production`.
The image is not rebuild, but new tags are added:

* production
* latest

This way you can reference the latest stable production version.

The list of tags can be found on [Docker Hub](https://hub.docker.com/r/amdatu/amdatu-kubernetes-deploymentctl-backend/tags/).
