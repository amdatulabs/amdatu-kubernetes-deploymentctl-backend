/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.kubernetes.rest;

import io.fabric8.kubernetes.api.model.Secret;
import io.vertx.core.json.Json;
import org.amdatu.deploymentctl.web.WebConstants;
import org.amdatu.deploymentctl.web.auth.AuthUtils;
import org.amdatu.deploymentctl.web.auth.WebUser;
import org.amdatu.kubernetes.Kubernetes;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.stream.Collectors;

@Component(provides = Object.class)
@Property(name = AmdatuWebRestConstants.JAX_RS_APPLICATION_NAME, value = WebConstants.JAX_RS_APPLICATION_NAME)
@Path("/secrets/{namespace}")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KubernetesResource {

	@ServiceDependency
	private volatile Kubernetes kubernetes;

	@GET
	public void listSecrets(
			@PathParam("namespace") String namespace,
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		if (!AuthUtils.hasRole(namespace, webuser)) {
			response.resume(new ForbiddenException());
			return;
		}

		kubernetes.listSecrets(namespace)
			.map(secretList -> secretList.getItems())
			.map(secrets -> secrets.stream()
					.filter(secret ->
						!secret.getType().equals("kubernetes.io/service-account-token") &&
						!secret.getType().equals("kubernetes.io/tls"))
					.collect(Collectors.toList()))
			.map(Json::encode)
			.subscribe(response::resume, response::resume);
	}

	@POST
	public void setSecret(
			@PathParam("namespace") String namespace,
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response,
			Secret secret
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		if (!AuthUtils.hasRole(namespace, webuser)) {
			response.resume(new ForbiddenException());
			return;
		}

		kubernetes.setSecret(namespace, secret)
				.subscribe(response::resume, response::resume);
	}

	@DELETE
	@Path("/{name}")
	public void deleteSecret(
			@PathParam("namespace") String namespace,
			@PathParam("name") String name,
			@Context HttpServletRequest request,
			@Suspended AsyncResponse response
	) {

		WebUser webuser = (WebUser)request.getAttribute(WebUser.class.getName());
		if (!AuthUtils.hasRole(namespace, webuser)) {
			response.resume(new ForbiddenException());
			return;
		}

		kubernetes.deleteSecret(namespace, name)
				.subscribe(response::resume, response::resume);
	}

}
