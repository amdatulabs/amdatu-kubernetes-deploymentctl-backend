/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.configuration.configadmin;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import org.amdatu.deploymentctl.configuration.Config;
import org.amdatu.deploymentctl.configuration.Description;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.base.CaseFormat;


@Component
public class Configurator{

	private volatile Config m_config;
	
	@ServiceDependency
	private volatile ConfigurationAdmin m_configAdmin;
	
	@Inject
	private volatile DependencyManager m_dependencyManager;
	
	@Start
	public void start() {
		try {
			byte[] configBytes = Files.readAllBytes(Paths.get("config.yml"));
			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			m_config = mapper.readValue(configBytes, Config.class);

			replaceEnvVars();
			printConfig();
			validateConfig();
			
			handleConfigAdmin();
			
			m_dependencyManager.add(
					m_dependencyManager.createComponent()
					.setInterface(Config.class.getName(), null)
					.setImplementation(m_config));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void handleConfigAdmin() {
		try {
			Configuration configuration = m_configAdmin.getConfiguration("org.amdatu.kubernetes", null);
			Dictionary<String, Object> properties = new Hashtable<>();
			properties.put("kubernetesurl", m_config.kubernetesUrl);
			
			configuration.update(properties);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void validateConfig() {
		StringBuilder b = new StringBuilder("======== Missing configuration ========\n");
		AtomicBoolean valid = new AtomicBoolean(true);
		
		Arrays.stream(Config.class.getFields()).forEach(f -> {
			try {
				if(f.get(m_config) == null) {
					valid.set(false);
					b.append(getDescription(f));
					b.append(" - ").append(f.getName()).append("\n\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
		if(!valid.get()) {
			System.err.println(b.toString());
			System.exit(1);
		}
		
	}

	private void replaceEnvVars() {
		StringBuilder b = new StringBuilder("======== Environment variables ========\n");
		
		Arrays.stream(Config.class.getFields()).forEach(f -> {
			String envVarName = getEnvName(f);
			String value = System.getenv(envVarName);
			if(value == null) {
				value = System.getProperty(envVarName);
			}
			
			if(value != null) {
				try {
					if(f.getType().equals(Integer.TYPE)) {
						f.setInt(m_config, Integer.parseInt(value));
					} else if(f.getType().equals(Long.TYPE)) {
						f.setLong(m_config, Long.parseLong(value));
					} else if(f.getType().equals(Double.TYPE)) {
						f.setDouble(m_config, Double.parseDouble(value));
					} else if(f.getType().equals(Boolean.TYPE)) {
						f.setBoolean(m_config, Boolean.parseBoolean(value));
					} else {
						f.set(m_config, value);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			b.append(envVarName).append(": ").append(value).append("\n");
		});
		
		b.append("\n");
		
		System.out.println(b.toString());
		
	}

	private String getEnvName(Field f) {
		return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, f.getName());
	}

	private void printConfig() {
		StringBuilder b = new StringBuilder("======== Configuration ========\n");
		Arrays.stream(Config.class.getFields()).forEach(f -> {
			Object value;
			try {
				value = f.get(m_config);
				b.append(getDescription(f));
				b.append(f.getName()).append(": ").append(value).append("\n\n");
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		b.append("\n");
		
		System.out.println(b.toString());
	}
	
	private String getDescription(Field f) {
		Description description = f.getAnnotation(Description.class);
		if(description == null) {
			return "";
		}
		
		StringBuilder b = new StringBuilder("# ");
		b.append(description.description()).append("\n");
		b.append("# example: ").append(description.example()).append("\n");
		b.append("# override with env var '").append(getEnvName(f)).append("'\n");
		return b.toString();
		
	}
}
