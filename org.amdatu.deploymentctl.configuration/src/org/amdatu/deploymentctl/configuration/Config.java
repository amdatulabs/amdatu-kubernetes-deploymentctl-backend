/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.configuration;

public class Config {
	@Description(description="Host of the Deployer", example="localhost")
	public String deployerHost;
	
	@Description(description="Port of the Deployer", example="8000")
	public int deployerPort;
	
	
	@Description(description="Url of the Kubernetes API", example="http://kubernetes-server:8080")
	public String kubernetesUrl;

	@Description(description="Redis host", example="localhost")
	public String redisHost;
	
	@Description(description="Port this application listens on", example="8080")
	public int port;
	
	@Description(description="KeyCloak public key", example="")
	public String keyCloakPublicKey;
}
