/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Description {
	String description() default "";
	String example() default "";
}
