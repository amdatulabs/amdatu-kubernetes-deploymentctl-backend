/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.redis.impl;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

import org.amdatu.deploymentctl.configuration.Config;
import org.amdatu.deploymentctl.redis.RedisService;

import io.vertx.redis.RedisOptions;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.redis.RedisClient;

@Component
public class RedisClientFactory implements RedisService {

	@ServiceDependency
	private volatile Vertx m_vertx;
	
	@ServiceDependency
	private volatile Config m_config;
	
	private volatile RedisClient m_client;
	
	@Start
	public void start() {
		RedisOptions config = new RedisOptions()
			    .setHost(m_config.redisHost);
		
		m_client = RedisClient.create(m_vertx, config);
	}
	
	
	@Override
	public RedisClient getClient() {
		return m_client;
	}

}
