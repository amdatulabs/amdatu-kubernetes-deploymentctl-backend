/**
 * Licensed under Apache License v2. See LICENSE for more information.
 */
package org.amdatu.deploymentctl.redis;

import io.vertx.rxjava.redis.RedisClient;

public interface RedisService {

	RedisClient getClient();
}
