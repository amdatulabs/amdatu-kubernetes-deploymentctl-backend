#!/bin/bash

# this should be called within an Java docker container during manual build OR from the bitbucket pipeline

# fail immedialtely
set -e
set -o pipefail

# build
./gradlew clean build export

IMAGE=amdatu/amdatu-kubernetes-deploymentctl-backend:latest
cd run
docker build -t "$IMAGE" .
docker login --username="$DOCKER_USER" --password="$DOCKER_PASSWORD"
docker push "$IMAGE"
